routes.json: process.py master_routes.json
	./process.py

%.json: %.ql
	wget -O $@ --post-file=$< http://overpass-api.de/api/interpreter

.PHONY: server
server:
	python3 -m http.server 8000

.PHONY: clean
clean:
	rm -f master_routes.json
	rm -f routes.json

.PHONY: cache-clean
cache-clean: clean
	rm -rf cache
