# bus_route_checker #

A tool to check OSM bus routes for the most important aspects of the
"public_transport" mapping scheme ((1), (2)).

### Usage ###

* Adapt the "master_routes.ql" file for your needs, i.e. change the
  Overpass query so that it returns the "route_master"-relations of the
  area you are interested in. You can use "overpass turbo" (3) to
  develop your query.

* Run `make`. This will first run the Overpass query and download the results,
  and then run the `process.py` script that will download further information
  and do the analysis.

* Open the "index.html" file in your web browser and wait for the routes to
  load. If you use Chrome, your browser will block requests to files in
  the local files system (see (4)). You can use `make server` to run a simple
  webserver and then connect to "localhost:8000" to see the results.

### Checks ###

  - "route_master"-relation: check the existence of the most elementary tags
    like `name`, `ref`, `network`, or `operator`
  - "route"-relation: check the existence of the most elementary tags
    like `name`, `ref`, `network`, `operator`, `from`, or `to`
  - check if the same tags of the "router_master" and the "route" relations
    also contain the same values
  - "route"-relation: check the tags of the contained platform and stop members
  - "route"-relation: check the maximum distance between a platform and its
    corresponding stop
  - "route"-relation: do a basic connectivity check on the ways in the relation
    (not perfect, but works in most cases and doesn't produce false positives)

`platform` and `stop` members have to be nodes, all way members of the
route-relations are currently treated as highways.
Routes that contain errors are expanded when the generated html file is opened
and the errors are highlighted in red.

### License ###

This project is licensed under the terms of the GNU General Public License
(GPL), version 2 or later. See the file "websrc/licenses" for the license
status of the used third party projects.

### References ###

  1. http://wiki.openstreetmap.org/wiki/Key:public_transport
  2. http://wiki.openstreetmap.org/wiki/Public_transport
  3. http://overpass-turbo.eu/
  4. https://code.google.com/p/chromium/issues/detail?id=47416
