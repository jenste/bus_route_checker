##
## This file is part of the bus_route_checker project.
##
## Copyright (C) 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
##

class ConnectivityError(RuntimeError):
    pass

class EmptyError(ConnectivityError):
    pass

class NoConnectionError(ConnectivityError):
    def __init__(self, index):
        # The way segments at 'index' and 'index + 1' are not connected.
        self.index = index

class WrongDirectionError(ConnectivityError):
    def __init__(self, index):
        # The way segments at 'index' is connected to both adjacent ways,
        # but both of them are connected to the same end.
        self.index = index

class NoRoundaboutConnectionError(ConnectivityError):
    def __init__(self, rindex, windex):
        # The way at index 'windex' is not connected to the roundabout
        # at index 'rindex'.
        self.rindex = rindex
        self.windex = windex

def check(ids, waysf):
    '''Check if the ways in a route are connected.

    'ids': ordered list of way IDs
    'waysf': a function that returns the list of points for a given way ID

    The check is not perfect, but it catches the most common errors.
    '''

    if not ids:
        raise EmptyError('contains no ways')

    if any(not waysf(wid) for wid in ids):
        raise EmptyError('constains an empty way')

    # for each two successive way elements wayi and wayj this list contains a
    # tuple (a, b, c, d) where:
    #
    # a true if wayi.last  == wayj.first
    # b true if wayi.last  == wayj.last
    # c true if wayi.first == wayj.first
    # d true if wayi.first == wayj.last
    con = []

    for idi, idj in zip(ids[:-1], ids[1:]):
        wayi = waysf(idi)
        wayj = waysf(idj)

        a = wayi[-1] == wayj[0]
        b = wayi[-1] == wayj[-1]
        c = wayi[0] == wayj[0]
        d = wayi[0] == wayj[-1]

        con.append((a, b, c, d))

    # set to 'True' if the way at the index is a roundabout
    rnd = []
    for wid in ids:
        way = waysf(wid)
        rnd.append(way[0] == way[-1])

    # check if two successive ways are connected
    for idx, x in enumerate(con):
        a, b, c, d = x

        # first, check if one of the ways is a roundabout, in that case it's
        # sufficient if one endpoint of the other way is contained in the roundabout
        if rnd[idx]:
            roundabout = waysf(ids[idx])
            way = waysf(ids[idx + 1])

            if not ((way[0] in roundabout) or way[-1] in roundabout):
                raise NoRoundaboutConnectionError(idx, idx + 1)
        elif rnd[idx + 1]:
            roundabout = waysf(ids[idx + 1])
            way = waysf(ids[idx])

            if not ((way[0] in roundabout) or way[-1] in roundabout):
                raise NoRoundaboutConnectionError(idx + 1, idx)
        elif not (a or b or c or d):
            raise NoConnectionError(idx)

    # check if three successive ways can be connected
    # (even if the one in the middle is reversed)
    for idx, x in enumerate(zip(con[:-1], con[1:])):
        # Avoid false positives if the a/b/c/d truth values are not disjoint.
        # That happens if the same way occurs twice (as in the case of a
        # no exit street) or if a small loop with only two streets occurs.
        if x[0].count(True) > 1:
            continue

        (a0, b0, c0, d0), (a1, b1, c1, d1) = x

        # skip if one of the ways is a roundabout
        if any([rnd[idx], rnd[idx + 1], rnd[idx + 2]]):
            continue

        if a0 or c0:
            # the way in the middle is not reversed
            if not (a1 or b1):
                raise WrongDirectionError(idx + 1)

        if b0 or d0:
            # the way in the middle is reversed
            if not (c1 or d1):
                raise WrongDirectionError(idx + 1)
