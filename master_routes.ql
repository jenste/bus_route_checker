[out:json];

rel[route_master=bus][network="Verkehrsverbund Vorarlberg"] -> .masters;

// include the "route" relations into the output,
// so that we have their timestamps for caching
rel(r.masters) -> .routes;

(.masters; .routes); out meta;
