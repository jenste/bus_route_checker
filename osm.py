##
## This file is part of the bus_route_checker project.
##
## Copyright (C) 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
##

import calendar
import sys
import time

def parse_timestamp(s):
    '''Tries to parse the string 's' as a time value as used by the OSM API
    (ISO 8601 with time zone designator 'Z').
    '''

    ts = time.strptime(s, '%Y-%m-%dT%H:%M:%SZ')
    return calendar.timegm(ts) # to seconds since epoch

class Node:
    def __init__(self, xml):
        self.id = int(xml.attrib['id'])
        self.position = (float(xml.attrib['lat']), float(xml.attrib['lon']))
        self.tags = dict((t.attrib['k'], t.attrib['v']) for t in xml.xpath('./tag'))

class Way:
    def __init__(self, xml):
        self.id = int(xml.attrib['id'])
        self.tags = dict((t.attrib['k'], t.attrib['v']) for t in xml.xpath('./tag'))
        self.nodes = list(int(n.attrib['ref']) for n in xml.xpath('./nd'))

class Relation:
    def __init__(self, overpass_json=None, osm_xml=None):
        if osm_xml is None and overpass_json is None:
            raise ValueError('neither osm_xml nor overpass_json provided')

        self.id = None
        self.timestamp = None
        self.tags = {}

        # ordered members (from the OSM API data)
        # [(id, role), ...]
        self.oways = []
        self.onodes = []

        # unordered relation members (from the Overpass API data)
        # [(id, role), ...]
        self.urelations = []

        if overpass_json:
            self.parse_overpass(overpass_json)

        if osm_xml is not None:
            self.parse_xml(osm_xml)

    def parse_overpass(self, js):
        assert 'type' in js, '"type" not found in JSON'
        assert js['type'] == 'relation', 'wrong "type" in JSON'

        assert 'id' in js, '"id" not found in JSON'
        self.id = js['id']

        if 'members' in js:
            for member in js['members']:
                assert 'type' in member, '"type" not found in JSON'
                assert member['type'] in ('relation', 'way', 'node'), 'unknown value for "type"'

                m = (member['ref'], member['role'])
                if member['type'] == 'relation':
                    self.urelations.append(m)
                else:
                    sys.stderr.write('ignoring {} in JSON\n'.format(member['type']))

        if 'tags' in js:
            self.tags = js['tags']

    def parse_xml(self, xml):
        self.id = int(xml.attrib['id'])
        self.tags = dict((t.attrib['k'], t.attrib['v']) for t in xml.xpath('./tag'))

        ways = xml.xpath("./member[@type='way']")
        self.oways.extend([(int(w.attrib['ref']), w.attrib['role']) for w in ways])

        nodes = xml.xpath("./member[@type='node']")
        self.onodes.extend([(int(n.attrib['ref']), n.attrib['role']) for n in nodes])

        self.timestamp = parse_timestamp(xml.attrib['timestamp'])
