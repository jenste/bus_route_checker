#!/usr/bin/env python3

##
## This file is part of the bus_route_checker project.
##
## Copyright (C) 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
##

import collections
import connectivity
import io
import json
import lxml.etree as ET
import math
import multiprocessing
import os
import osm
import relationcache
import requests
import sys
import time

def node_link(nid, text):
    return '<a href="http://www.openstreetmap.org/node/{}">{}</a>'.format(nid, text)

def way_link(wid, text):
    return '<a href="http://www.openstreetmap.org/way/{}">{}</a>'.format(wid, text)

platform_roles = ('platform', 'platform_exit_only', 'platform_entry_only')
stop_roles = ('stop', 'stop_exit_only', 'stop_entry_only')

def p_role(r):
    return r in platform_roles

def s_role(r):
    return r in stop_roles

def dedup(seq):
    '''Remove duplicates while preserving the order.

    (http://www.peterbe.com/plog/uniqifiers-benchmark)
    '''

    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def distance(c1, c2):
    '''Calculates the distance between two coordinates.'''

    # http://www.movable-type.co.uk/scripts/latlong.html

    lat = 0
    lon = 1

    r = 6371000
    phi1 = c1[lat] * math.pi / 180
    phi2 = c2[lat] * math.pi / 180
    dphi = (c2[lat] - c1[lat]) * math.pi / 180
    dlam = (c2[lon] - c1[lon]) * math.pi / 180

    a = pow(math.sin(dphi/2), 2) + \
        math.cos(phi1) * math.cos(phi2) * \
        pow(math.sin(dlam/2), 2)

    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a));
    return r * c

def extract_route_masters(js, augment_relation=None):
    '''Go through the JSON from the Overpass API and extract the route_master
    relations and the timestamps of the route relations.
    '''

    masters = []
    timestamps = {}

    assert 'elements' in js, '"elements" not found in JSON'
    for ele in js['elements']:
        assert 'type' in ele, '"type" not found in JSON'
        assert ele['type'] in ('relation', 'way', 'node'), 'unknown value for "type"'

        if ele['type'] == 'relation':
            if 'tags' in ele:
                if 'route_master' in ele['tags']:
                    masters.append(osm.Relation(overpass_json=ele))
                elif 'route' in ele['tags'] and 'timestamp' in ele:
                    try:
                        timestamps[ele['id']] = osm.parse_timestamp(ele['timestamp'])
                    except Exception as e:
                        sys.stderr.write('unable to parse timestamp "{}"'.format(ele['timestamp']))
        else:
            sys.stderr.write('ignoring {} in JSON\n'.format(ele['type']))

    return (masters, timestamps)

node_cache = {}
way_cache = {}

def parse_relations_xml(relation_id, xml):
    '''Parses a relation XML file from the OSM API.

    The contained nodes/ways are put in the global 'node_cache'/'way_cache'
    variables, and the relation with ID 'relation_id' is returned.
    '''

    parser = ET.XMLParser(remove_blank_text=True)
    et = ET.parse(io.BytesIO(xml), parser)
    root = et.getroot()

    for nxml in root.xpath('/osm/node'):
        node = osm.Node(nxml)
        node_cache[node.id] = node

    for wxml in root.xpath('/osm/way'):
        way = osm.Way(wxml)
        way_cache[way.id] = way

    # we are only interested in this one relation
    rxml = root.xpath("/osm/relation[@id='{}']".format(relation_id))
    assert len(rxml) == 1, 'relation with id {} not found'.format(relation_id)
    return rxml[0]

def simplify_polyline(pl):
    '''Merge successive ways.'''

    # filter out empty parts
    pl = [l for l in pl if l]

    if len(pl) < 2:
        # nothing to do
        return pl

    done = [pl[0]]

    for l in pl[1:]:
        last = done[-1]
        if last[-1] == l[0]:
            l.popleft()
            last.extend(l)
        elif last[-1] == l[-1]:
            l.pop()
            l.reverse()
            last.extend(l)
        elif last[0] == l[0]:
            l.popleft()
            last.extendleft(l)
        elif last[0] == l[-1]:
            l.pop()
            l.reverse() # because 'extendleft()' reverses again
            last.extendleft(l)
        else:
            # no match
            done.append(l)

    return done

def maxdist(sid, pid):
    '''Returns the maximum allowed distance between a stop and a platform,
    taking into account the values in defined in a whitelist file.'''

    if (sid, pid) in distance_whitelist:
        return distance_whitelist[(sid, pid)]

    # use default value
    return 20

def check_stop_platform(route):
    '''Check if role="stop" nodes are followed by role="platform".'''

    pairs = []

    # role we are expecting for the next node
    state = 'stop'

    for nid, role in route.onodes:
        if not role in stop_roles + platform_roles:
            yield '{} has unknown role "{}"'.format(node_link(nid, 'node'), role)
        else:
            if state == 'stop' and s_role(role):
                last = nid
                state = 'platform'
            elif state == 'platform' and p_role(role):
                state = 'stop'
                pairs.append((last, nid))
            else:
                yield '{} doesn\'t have "{}" role'.format(node_link(nid, 'node'), state)

    if state != 'stop':
        yield 'last "stop" node doesn\'t have a "platform"'

    for sid, pid in pairs:
        sc = node_cache[sid].position
        pc = node_cache[pid].position
        dist = distance(sc, pc)

        if dist > maxdist(sid, pid):
            yield 'big distance between {} and {}'.format(
                node_link(sid, 'stop'), node_link(pid, 'platform'))

def check_stops_on_route(route):
    '''Check if the role="stop" nodes are actually on the route.'''

    allnodes = [n for wayid, role in route.oways for n in way_cache[wayid].nodes]
    allnodes = set(allnodes)

    stops = [nid for nid, role in route.onodes if s_role(role)]
    for s in stops:
        if not s in allnodes:
            yield '{} isn\'t part of the route'.format(node_link(s, 'stop node'))

def check_fist_last_stop(route):
    '''Check if the first stop is the first node of the first way, and if the
    last stop is the last node of the last way.'''

    if not len(route.oways):
        return

    # don't care how the ways are oriented, get the first and the last node
    # of the first and the last route element

    firstway = way_cache[route.oways[0][0]]
    if len(firstway.nodes):
        fnodes = (firstway.nodes[0], firstway.nodes[-1])
    else:
        fnodes = None

    lastway = way_cache[route.oways[-1][0]]
    if len(lastway.nodes):
        lnodes = (lastway.nodes[0], lastway.nodes[-1])
    else:
        lnodes = None

    stops = [nid for nid, role in route.onodes if s_role(role)]
    if not len(stops):
        return

    if fnodes != None and stops[0] not in fnodes:
        yield '{} isn\'t one of the ends of the first way ({}, {})'.format(
            node_link(stops[0], 'first stop'),
            node_link(fnodes[0], 'endpoint'),
            node_link(fnodes[1], 'endpoint'))
    if lnodes != None and stops[-1] not in lnodes:
        yield '{} isn\'t one of the ends of the last way ({}, {})'.format(
            node_link(stops[-1], 'last stop'),
            node_link(fnodes[0], 'endpoint'),
            node_link(fnodes[1], 'endpoint'))

def check_node_tags(route):
    '''Check for errors in the tags of nodes with role "stop" and "platform".'''

    for nid, role in route.onodes:
        if role in stop_roles + platform_roles:
            tags = node_cache[nid].tags
            if s_role(role):
                nl = node_link(nid, 'stop node')
                #if 'name' in tags:
                #    yield '{} has "name" tag'.format(nl)

                if not 'public_transport' in tags:
                    yield '{} has no "public_transport" tag'.format(nl)
                elif tags['public_transport'] != 'stop_position':
                    yield '{} has wrong value for "public_transport" tag'.format(nl)

                if 'route' in route.tags and \
                    route.tags['route'] in ('bus', 'train', 'tram'):
                    pt = route.tags['route']

                    if not pt in tags:
                        yield '{} has no "{}" tag'.format(nl, pt)
                    elif tags[pt] != 'yes':
                        yield '{} has wrong value for "{}" tag'.format(nl, pt)
            else:
                nl = node_link(nid, 'platform node')
                if not 'name' in tags:
                    yield '{} has no "name" tag'.format(nl)

                if not 'public_transport' in tags:
                    yield '{} has no "public_transport" tag'.format(nl)
                elif tags['public_transport'] != 'platform':
                    yield '{} has wrong value for "public_transport" tag'.format(nl)

                if 'route' in route.tags and route.tags['route'] == 'bus':
                    if not 'highway' in tags:
                        yield '{} has no "highway" tag'.format(nl)
                    elif tags['highway'] != 'bus_stop':
                        yield '{} has wrong value for "highway" tag'.format(nl)

def check_connectivity(route):
    '''Check if all parts of the route are connected with each other.'''

    # the IDs of the ways
    ids = (wid for wid, role in route.oways)
    ids = list(ids)

    try:
        connectivity.check(ids, lambda i: way_cache[i].nodes)
    except connectivity.NoConnectionError as e:
        yield '{} and {} are not connected'.format(
                way_link(ids[e.index], 'way'),
                way_link(ids[e.index + 1], 'way'))
    except connectivity.WrongDirectionError as e:
        yield '{}, {}, and {} are connected in the wrong way'.format(
                way_link(ids[e.index - 1], 'way'),
                way_link(ids[e.index], 'way'),
                way_link(ids[e.index + 1], 'way'))
    except connectivity.NoRoundaboutConnectionError as e:
        yield '{} is not connected to the {}'.format(
                way_link(ids[e.windex], 'way'),
                way_link(ids[e.rindex], 'roundabout'))

def process_route(master, route):
    sys.stdout.write(' processing route {}'.format(route.id))
    if 'name' in route.tags:
        sys.stdout.write(' ({})'.format(route.tags['name']))
    sys.stdout.write('\n')

    #
    # go through the ordered ways in the relation from the OSM API,
    # and build a list of coordinates
    #

    # 'route.oways' contains the way IDs, convert to a list of lists of node IDs
    ws = [way_cache[wid].nodes for wid, role in route.oways]
    # use a deque, so that 'simplify_polyline()' can work effectively
    ws = [collections.deque(w) for w in ws]
    # merge as many ways as possible
    ws = simplify_polyline(ws)
    # use the coordinates
    polylines = [[node_cache[n].position for n in w] for w in ws]

    errors = []

    def check_tag_existance(tag):
        '''Issues an error message if the 'tag' does not exists. Returns
        'True' if it exists, 'False' otherwise.'''
        if not tag in route.tags:
            errors.append('route has no "{}" tag'.format(tag))
            return False
        return True

    def check_tag_difference(rtag, mtag=None):
        if check_tag_existance(rtag):
            mtag = rtag if mtag == None else mtag
            if mtag in master.tags and master.tags[mtag] != route.tags[rtag]:
                msg = '"{}" tag differs from master\'s {} tag<br>("route: {}", master: "{}")'.format(
                        rtag, mtag, route.tags[rtag], master.tags[mtag])
                errors.append(msg)

    def check_ref_tag():
        if check_tag_existance('ref'):
            if 'ref' in master.tags:
                mrefs = route.tags['ref'].split('/')
                ref = route.tags['ref']
                if not any(ref == mr for mr in mrefs):
                    msg = 'route\'s "ref" tag not part of master\'s "ref" tag'
                    errors.append(msg)

    check_tag_existance('name')
    check_tag_existance('from')
    check_tag_existance('to')
    if check_tag_existance('public_transport:version'):
        if route.tags['public_transport:version'] != '2':
            errors.append('wrong public_transport:version')
    check_tag_difference('network')
    check_tag_difference('operator')
    check_tag_difference('route', 'route_master')
    check_ref_tag()

    errors.extend(check_stop_platform(route))
    errors.extend(check_stops_on_route(route))
    errors.extend(check_fist_last_stop(route))
    errors.extend(check_node_tags(route))
    errors.extend(check_connectivity(route))

    # the IDs of the platforms contained in this route
    platforms = [nid for nid, role in route.onodes if p_role(role)]

    return {
        'id': route.id,
        'tags': route.tags,
        'polylines': polylines,
        'errors': dedup(errors),
        'platforms': platforms
    }

def process_route_master(master):
    '''Processes a route master relation, the routes should already
    be available in the '.routes' attribute of the master object.'''

    sys.stdout.write('processing route_master {}'.format(master.id))
    if 'name' in master.tags:
        sys.stdout.write(' ({})'.format(master.tags['name']))
    sys.stdout.write('\n')

    routes_output = [process_route(master, route) for route in master.routes]
    routes_output.sort(key=lambda r: r['tags']['name'] if 'name' in r['tags'] else '')

    errors = []

    def check_tag_existance(tag):
        if not tag in master.tags:
            errors.append('route_master has no "{}" tag'.format(tag))
            return False
        return True

    check_tag_existance('name')
    check_tag_existance('ref')
    check_tag_existance('network')
    check_tag_existance('operator')

    if 'route' in master.tags:
        errors.append('route_master has "route" tag')

    return {
        'id': master.id,
        'tags': master.tags,
        'routes': routes_output,
        'errors': dedup(errors)
    }

def collect_platforms(master_routes):
    '''Returns a dictionary containing all platforms from all routes.'''

    platforms = {}

    for master in master_routes:
        for route in master.routes:
            for nid, role in route.onodes:
                if p_role(role):
                    node = node_cache[nid]
                    name = node.tags['name'] if 'name' in node.tags else ''
                    platforms[nid] = { 'name': name, 'position': node.position }

    return platforms

def download_relation_xml(arg):
    '''Function to downlaod the full relation XML from the OSM API in a subprocess.'''
    master_index, relation_id = arg

    url = 'https://www.openstreetmap.org/api/0.6/relation/{}/full'.format(relation_id)
    r = requests.get(url)

    return (master_index, relation_id, r.content)

if __name__ == '__main__':
    # load distance whitelist
    distance_whitelist = {}
    if os.path.isfile('distance.whitelist'):
        with open('distance.whitelist') as f:
            for obj in json.load(f):
                distance_whitelist[(obj['s'], obj['p'])] = obj['d']

    with open('master_routes.json') as f:
         overpass = json.load(f)

    (route_masters, timestamps) = extract_route_masters(overpass)

    # add an additional attribute to hold the routes
    for m in route_masters:
        m.routes = []

    work = [(midx, rid) for midx, m in enumerate(route_masters)
                        for rid, role in m.urelations if role == '']

    def add_relation(mindex, rid, xml):
        '''Creates a Relation object from 'xml' and adds it to the routes
        of the master relation (corresponding to 'mindex').

        Returns the route relation.'''
        master = route_masters[mindex]

        # parse the XML output, create a Relation object for the route
        route_xml = parse_relations_xml(rid, xml)
        route = osm.Relation(osm_xml=route_xml)

        # save the route for later
        master.routes.append(route)

        return route

    dl_work = []
    for midx, rid in work:
        rxml = None
        if rid in timestamps:
            rxml = relationcache.lookup(rid, timestamps[rid])

        if rxml == None:
            # not in cache, download it
            dl_work.append((midx, rid))
        else:
            print('using cached relation {}'.format(rid))
            add_relation(midx, rid, rxml)

    # download the remaining relatins that were not found in the cache
    with multiprocessing.Pool(processes=2) as pool:
        it = pool.imap(download_relation_xml, dl_work)
        while True:
            try:
                midx, rid, rxml = next(it)
            except StopIteration:
                break
            except Exception as e:
                sys.stderr.write('error downloading a relation: {}\n'.format(e))
                continue

            print('received relation {}'.format(rid))

            try:
                route = add_relation(midx, rid, rxml)
            except Exception as e:
                sys.stderr.write('error processing a relation: {}\n'.format(e))
                continue

            relationcache.add(rid, route.timestamp, rxml)

    master_output = [process_route_master(rm) for rm in route_masters]
    master_output.sort(key=lambda rm: rm['tags']['name'] if 'name' in rm['tags'] else '')

    platforms_output = collect_platforms(route_masters)

    metadata = {
        'time': time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
    }

    output = {
        'route_masters': master_output,
        'platforms': platforms_output,
        'metadata': metadata
    }

    with open('routes.json', 'w') as out:
        json.dump(output, out, indent=2, sort_keys=True)
