##
## This file is part of the bus_route_checker project.
##
## Copyright (C) 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
##

# name of the directory used to cache relations
CACHEDIR = 'cache'

import os
import sys

_havecache = True

try:
    if not os.path.isdir(CACHEDIR):
        os.makedirs(CACHEDIR)
except Exception as e:
    sys.stderr.write('unable to create cache directory: {}\n'.format(e))
    _havecache = False

def _name(rid):
    '''Returns the name that the relation with ID 'rid' has in the cache.'''
    return os.path.join(CACHEDIR, 'relation-' + str(rid) + '.xml')

def lookup(rid, timestamp):
    '''Checks if a cached version of the relation with ID 'rid' that is at
    least as young as 'timestamp' is in the cache.

    Returns the binary content of the file, or 'None' if the relation is
    not available.
    '''

    if not _havecache:
        return

    try:
        fp = _name(rid)

        mtime = os.path.getmtime(fp)
        if timestamp > mtime:
            # file too old
            return None

        with open(fp, 'rb') as f:
            return f.read()
    except:
        return None

def add(rid, timestamp, data):
    '''Add the relation with ID 'rid' to the cache directory.'''

    if not _havecache:
        return

    try:
        fp = _name(rid)

        with open(fp, 'wb') as f:
            f.write(data)

        os.utime(fp, (timestamp, timestamp))
    except Exception as e:
        sys.stderr.write('error adding relation to cache: {}\n'.format(e))
