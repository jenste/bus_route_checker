#!/usr/bin/env python3

import connectivity
import collections
import unittest
import process

def sp(l):
    '''Wrapper around 'process.simplify_polyline()' that converts between
    lists and collections.deques and back again.'''
    d = [collections.deque(i) for i in l]
    d = process.simplify_polyline(d)
    return [list(i) for i in d]

class Test_simplify_polyline(unittest.TestCase):
    def test_nop(self):
        self.assertEqual(sp([]), [])
        self.assertEqual(sp([[]]), [])
        self.assertEqual(sp([[], []]), [])
        self.assertEqual(sp([[1]]), [[1]])
        self.assertEqual(sp([[1], []]), [[1]])

        pl = [[1, 2, 3], [4, 5, 6]]
        self.assertEqual(sp(pl), pl)

    def test_append(self):
        pl = [[1, 2, 3], [3, 4, 5]]
        self.assertEqual(sp(pl), [[1, 2, 3, 4, 5]])

        pl = [[1, 2, 3], [3, 4, 5], [7, 8], [8, 9], [9]]
        self.assertEqual(sp(pl), [[1, 2, 3, 4, 5], [7, 8, 9]])

    def test_append_reversed(self):
        pl = [[1, 2, 3], [5, 4, 3]]
        self.assertEqual(sp(pl), [[1, 2, 3, 4, 5]])

    def test_prepend(self):
        pl = [[3, 4, 5], [1, 2, 3]]
        self.assertEqual(sp(pl), [[1, 2, 3, 4, 5]])

    def test_prepend_reversed(self):
        pl = [[3, 4, 5], [3, 2, 1]]
        self.assertEqual(sp(pl), [[1, 2, 3, 4, 5]])

        pl = [[3, 2, 1], [3, 4, 5]]
        self.assertEqual(sp(pl), [[5, 4, 3, 2, 1]])

    def test_disjunct(self):
        pl = [[1], [2], [4, 5, 6]]
        self.assertEqual(sp(pl), pl)

class Test_connectivity(unittest.TestCase):
    # the ways we use for the tests
    ways = {
        0: ['a', 'b', 'c'],
        1: ['c', 'd', 'e'],
        2: ['e', 'f', 'g'],
        3: ['g', 'h', 'i', 'j', 'g'],
        4: ['e', 'j'],
        5: ['i', 'k', 'a'],
        6: [],
        7: ['e', 'l', 'c'],
        8: ['a', 'b', 'd', 'e']
    }

    def call(self, ways):
        return connectivity.check(ways, lambda i: Test_connectivity.ways[i])

    def test_empty(self):
        with self.assertRaisesRegex(connectivity.EmptyError, 'contains no ways'):
            self.call([])

    def test_empty_way(self):
        with self.assertRaisesRegex(connectivity.EmptyError, 'constains an empty way'):
            self.call([6])

        with self.assertRaisesRegex(connectivity.EmptyError, 'constains an empty way'):
            self.call([3, 6])

    def test_no_connection(self):
        with self.assertRaises(connectivity.NoConnectionError) as cm:
            self.call([0, 2])
        self.assertEqual(cm.exception.index, 0)

        with self.assertRaises(connectivity.NoConnectionError) as cm:
            self.call([5, 0, 4])
        self.assertEqual(cm.exception.index, 1)

    def test_okay(self):
        self.assertIsNone(self.call([0]))
        self.assertIsNone(self.call([0, 1]))
        self.assertIsNone(self.call([0, 1, 2]))

    def test_reverse(self):
        self.assertIsNone(self.call([0, 7]))
        self.assertIsNone(self.call([7, 2]))
        self.assertIsNone(self.call([7, 8]))

    def test_three_connected(self):
        self.assertIsNone(self.call([0, 7, 2]))

        with self.assertRaises(connectivity.WrongDirectionError) as cm:
            self.call([1, 2, 4])
        self.assertEqual(cm.exception.index, 1)

        with self.assertRaises(connectivity.WrongDirectionError) as cm:
            self.call([0, 1, 2, 4])
        self.assertEqual(cm.exception.index, 2)

    def test_no_roundabout_connection(self):
        with self.assertRaises(connectivity.NoRoundaboutConnectionError) as cm:
            self.call([1, 3])
        self.assertEqual(cm.exception.windex, 0)
        self.assertEqual(cm.exception.rindex, 1)

        with self.assertRaises(connectivity.NoRoundaboutConnectionError) as cm:
            self.call([3, 0])
        self.assertEqual(cm.exception.rindex, 0)
        self.assertEqual(cm.exception.windex, 1)

    def test_roundabout(self):
        self.assertIsNone(self.call([2, 3]))
        self.assertIsNone(self.call([3, 2]))
        self.assertIsNone(self.call([2, 3, 5]))
        self.assertIsNone(self.call([1, 4, 3, 2, 7, 0, 5, 3, 4]))

    def test_dead_end(self):
        self.assertIsNone(self.call([0, 1, 1, 0]))

    def test_small_loop(self):
        self.assertIsNone(self.call([1, 7, 0]))

class Test_distance_calculation(unittest.TestCase):
    def test_small_distance(self):
        a = (47.3115259, 9.6181612)
        b = (47.3115640, 9.6181796)

        d = process.distance(a, b)
        self.assertGreater(d, 4)
        self.assertLess(d, 5)

    def test_big_distance(self):
        a = (47.2459821, 9.6085662)
        b = (47.2482075, 9.6107419)

        d = process.distance(a, b)
        self.assertGreater(d, 295)
        self.assertLess(d, 300)

if __name__ == '__main__':
    unittest.main(verbosity=2)
